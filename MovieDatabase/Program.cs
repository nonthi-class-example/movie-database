﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string movieListText = Properties.Resources.MovieList;

            MovieDatabase mainDb = new MovieDatabase(movieListText);

            bool quit = false;
            while (!quit)
            {
                Console.WriteLine("Please specify a search mode:");
                Console.WriteLine("1. By Name");
                Console.WriteLine("2. By Director");
                Console.WriteLine("3. By Year");

                int searchMode = int.Parse(Console.ReadLine());

                Console.Write("Please enter your search term: ");
                string searchTerm = Console.ReadLine();

                Console.WriteLine("\n===== START SEARCH RESULT =====");

                List<Movie> searchResult = mainDb.SearchMovies(searchMode, searchTerm);
                foreach (Movie movie in searchResult)
                {
                    Console.WriteLine(movie.GetInfoString());
                }

                Console.WriteLine("===== END SEARCH RESULT =====\n");

                Console.Write("Search again? (y/n) ");

                string quitResponse = Console.ReadLine();
                if (quitResponse.ToLower().StartsWith("n"))
                {
                    quit = true;
                }
            }
        }
    }
}
