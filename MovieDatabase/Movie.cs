﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase
{
    internal class Movie
    {
        private string _name;
        public string Name => _name;

        private string _director;
        public string Director => _director;

        private int _year;
        public int Year => _year;

        public Movie(string name, string director, int year)
        {
            _name = name;
            _director = director;
            _year = year;
        }

        public string GetInfoString()
        {
            return $"{_name} ({_year}) by {_director}";
        }
    }
}
