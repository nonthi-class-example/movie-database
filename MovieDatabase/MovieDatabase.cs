﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieDatabase
{
    internal class MovieDatabase
    {
        private List<Movie> _movies = new List<Movie>();
        public ReadOnlyCollection<Movie> Movies => _movies.AsReadOnly();

        public MovieDatabase(string resourceText)
        {
            using (System.IO.StringReader reader = new System.IO.StringReader(resourceText))
            {
                while (true)
                {
                    string movieText = reader.ReadLine();
                    if (movieText == null) break;

                    string[] dataParts = movieText.Split('_');

                    _movies.Add(new Movie(dataParts[0], dataParts[1], int.Parse(dataParts[2])));
                }
            }
        }

        public List<Movie> SearchMovies(int searchMode, string searchTerm)
        {
            if (searchMode == 2)
            {
                return _movies.FindAll(x => x.Director.ToLower().Contains(searchTerm.ToLower()));
            }
            else if (searchMode == 3)
            {
                return _movies.FindAll(x => x.Year.ToString().Equals(searchTerm));
            }
            else
            {
                return _movies.FindAll(x => x.Name.ToLower().Contains(searchTerm.ToLower()));
            }
        }
    }
}
